#pragma once
#include "Framework.h"
#include "MoveableEntity.h"
#include <list>
#include <memory>
#include <cmath>

class Player {
public:
	~Player();

	bool setPlayerSprite(Sprite * player);
	bool setMouseSprite(Sprite * mouse);
	bool setBulletSprite(Sprite * bullet);
	void setNumAmmo(int numAmmo);
	Player();
	Player(const Player & par) = delete;
	Player & operator=(const Player & par) = delete;
	Sprite * getPlSprite();
	Sprite * getMsSprite();
	Sprite * getBulSprite();
	int getPlCenterX() const;
	int getPlCenterY() const;
	int getPlHeight() const;
	int getPlWidth() const;
	void findVelocity(double v0, double x0, double x1, double y0, double y1,
		double & velX, double & velY) const;
	void shotFired(int dx, int dy);
	std::list<std::unique_ptr<MoveableEntity>> bullets;
	bool UpPressed = false;
	bool DownPressed = false;
	bool LeftPressed = false;
	bool RightPressed = false;
	int X;
	int Y;
	int mX;
	int mY;
	int mXX;
	int mYY;
private:
	Sprite * _pl = nullptr;
	Sprite * _ms = nullptr;
	Sprite * _bul = nullptr;
	int _plHeight;
	int _plWidth;
	int _numAmmo;
};

