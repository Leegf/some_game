#include "MoveableEntity.h"

int MoveableEntity::countBulletsNum = 0;
int MoveableEntity::countEnemiesNum = 0;

MoveableEntity::MoveableEntity(Sprite * bul) 
{
	setSprite(bul);
}

MoveableEntity::~MoveableEntity() { }

void MoveableEntity::setSprite(Sprite * bul)
{
	_ent = bul;
	getSpriteSize(_ent, _width, _height);
}

Sprite * MoveableEntity::getSprite()
{
	return _ent;
}

int MoveableEntity::getWidth() const
{
	return _width;
}

int MoveableEntity::getHeight() const
{
	return _height;
}

void MoveableEntity::setDestination(int x, int y) 
{
	destX = x;
	destY = y;
}
