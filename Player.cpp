#include "Player.h"

Player::Player() {}

Player::~Player() {
	if (_pl)
		destroySprite(_pl);
	if (_ms)
		destroySprite(_ms);
	if (_bul)
		destroySprite(_bul);
}

bool Player::setPlayerSprite(Sprite * player)
{
	if (player == nullptr)
		return false;
	_pl = player;
	getSpriteSize(_pl, _plWidth, _plHeight);
	return true;
}
bool Player::setMouseSprite(Sprite * mouse)
{
	if (mouse == nullptr)
		return false;
	_ms = mouse;
	return true;
}
bool Player::setBulletSprite(Sprite * bul)
{
	if (bul == nullptr)
		return false;
	_bul = bul;
	return true;
}

void Player::setNumAmmo(int numAmmo)
{
	_numAmmo = numAmmo;
}

int Player::getPlWidth() const 
{
	return _plWidth;
}

int Player::getPlHeight() const 
{
	return _plHeight;
}

#define BULLET_SPEED 1.7
//dx, dy - bullet's destination
//sets up newly shot bullet
void Player::shotFired(int dx, int dy)
{
	int mouseWidth, mouseHeight;
	getSpriteSize(_ms, mouseWidth, mouseHeight);
	//X, Y - player's coordinates
	//do nothing if player is aiming at himself(suicide is not the solution)
	if (dx + mouseWidth >= X &&
		dx <= X + getPlWidth() &&
		dy + mouseHeight >= Y &&
		dy <= Y + getPlHeight())
		return;
	/* "If there is no ammo, then oldest bullet should
	disappear and new one should be spawned." */
	if (MoveableEntity::countBulletsNum == _numAmmo)
	{
		if (!bullets.empty())
			bullets.pop_back();
		MoveableEntity::countBulletsNum--;
	}
	bullets.push_back(std::unique_ptr<MoveableEntity>(new MoveableEntity(_bul)));
	MoveableEntity::countBulletsNum++;
	auto bul = std::prev(bullets.end()); //iterator
#define BUL (*bul)
	//new bullets would appear in a center of the player
	BUL->X = getPlCenterX();
	BUL->Y = getPlCenterY();
	BUL->setDestination(dx, dy);
	//following code calculates projections of velocity on x and y. 
	//mSpeedX and mSpeedY will contain calculated velocities.
	findVelocity(BULLET_SPEED, BUL->X, BUL->destX, BUL->Y, BUL->destY,
		BUL->mSpeedX, BUL->mSpeedY);
}

void Player::findVelocity(double v0, double x0, double x1, double y0, double y1, double & velX, double & velY) const
{
	double hypotenuse = sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
	double cos = (x1 - x0) / hypotenuse; //x projection of velocity
	double sin = (y1 - y0) / hypotenuse; //y projection of velocity
	velX = v0 * cos;
	velY = v0 * sin;
}

Sprite * Player::getPlSprite()
{
	return _pl;;
}

Sprite * Player::getMsSprite()
{
	return _ms;
}

Sprite * Player::getBulSprite()
{
	return _bul;
}

int Player::getPlCenterX() const
{
	return X + (getPlHeight() / 2);
}

int Player::getPlCenterY() const
{
	return Y + (getPlWidth() / 2);
}
