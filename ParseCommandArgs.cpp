#include "ParseCommandArgs.h"

ParseCommandArgs::ParseCommandArgs(int ac, char ** av)
{
	//for checking 800x800 and the like
	std::regex eSize(R"([1-9][0-9]*x[1-9][0-9]*)");
	//just for checking number
	std::regex eNumber(R"([1-9][0-9]*)");
	std::smatch m;
	std::string tmp;

	for (int i = 1; i < ac; i++) {
		if (strcmp("-window", av[i]) == 0) {
			if (i + 1 > ac)
				break;
			tmp = av[i + 1];
			if (std::regex_match(tmp, m, eSize)) {
				_windowWidth = _getParsedWidth(m);
				_windowHeight = _getParsedHeight(m);
			}
		}
		else if (strcmp("-map", av[i]) == 0) {
			if (i + 1 > ac)
				break;
			tmp = av[i + 1];
			if (std::regex_match(tmp, m, eSize)) {
				_mapWidth = _getParsedWidth(m);
				_mapHeight = _getParsedHeight(m);
			}
		}
		else if (strcmp("-num_enemies", av[i]) == 0) {
			if (i + 1 > ac)
				break;
			tmp = av[i + 1];
			if (std::regex_match(tmp, m, eNumber)) {
				_numEnemies = std::stoi(m.str());
			}
		}
		else if (strcmp("-num_ammo", av[i]) == 0) {
			if (i + 1 > ac)
				break;
			tmp = av[i + 1];
			if (std::regex_match(tmp, m, eNumber)) {
				_numAmmo = std::stoi(m.str());
			}
		}
		else if (strcmp("-f", av[i]) == 0)
			_fullscreen = true;
	}
}

ParseCommandArgs::~ParseCommandArgs() { }

int ParseCommandArgs::getWindowHeight() const
{
	return _windowHeight;
}

int ParseCommandArgs::getWindowWidth() const
{
	return _windowWidth;
}

int ParseCommandArgs::getMapWidth() const
{
	return _mapWidth;
}

int ParseCommandArgs::getMapHeight() const
{
	return _mapHeight;
}

int ParseCommandArgs::getNumEnemies() const
{
	return _numEnemies;
}

int ParseCommandArgs::getNumAmmo() const
{
	return _numAmmo;
}

int ParseCommandArgs::getFullscreen() const
{
	return _fullscreen;
}

int ParseCommandArgs::_getParsedWidth(std::smatch & m) const
{
	return (std::stoi(m.str()));
}

int ParseCommandArgs::_getParsedHeight(std::smatch & m) const
{
	return (std::stoi(m.str().substr(m.str().find("x") + 1)));
}
