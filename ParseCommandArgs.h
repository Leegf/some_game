#pragma once
#include <string.h>
#include <iostream>
#include <regex>
class ParseCommandArgs
{
public:
	ParseCommandArgs(int ac, char **av);
	ParseCommandArgs() = delete;
	ParseCommandArgs(ParseCommandArgs const & par) = delete;
	ParseCommandArgs & operator=(ParseCommandArgs const & par) = delete;
	~ParseCommandArgs();
	int getWindowHeight() const;
	int getWindowWidth() const;
	int getMapWidth() const;
	int getMapHeight() const;
	int getNumEnemies() const;
	int getNumAmmo() const;
	int getFullscreen() const;
private:

	int _getParsedWidth(std::smatch & m) const;
	int _getParsedHeight(std::smatch & m) const;
	int _windowHeight = 450;
	int _windowWidth = 800;
	int _mapWidth = 2000;
	int _mapHeight = 2000;
	int _numEnemies = 100;
	int _numAmmo = 100;
	bool _fullscreen = false;
};

