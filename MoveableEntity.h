#pragma once
#include "Framework.h"
class MoveableEntity
{
public:
	MoveableEntity() = delete;
	MoveableEntity(Sprite * bul);
	~MoveableEntity();
	void setSprite(Sprite * en);
	Sprite * getSprite();
	int getWidth() const;
	int getHeight() const;
	void setDestination(int x, int y);
	double X;
	double Y;
	double speed;
	double mSpeedX; //what distance in x entity will cover during iteration
	double mSpeedY; //what distance in y entity will cover during iteration
	double destX;
	double destY;
	static int countBulletsNum;
	static int countEnemiesNum;
private:
	Sprite * _ent = nullptr;
	int _width;
	int _height;
};

