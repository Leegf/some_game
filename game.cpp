#pragma comment(lib, "FrameworkRelease_x64")

#include "Framework.h"
#include <iostream>
#include "ParseCommandArgs.h"
#include "Player.h"
#include <list>
#include <cstdlib>
#include <cmath>
#include <memory>

/* Test Frameork realization */
class MyFramework : public Framework {

public:

	//sets up parameters from command line
	MyFramework(int height, int width, int mapHeight, int mapWidth, bool fullscreen,
		int numEnemies, int numAmmo) :
		_winHeight(height), _winWidth(width), _mapHeight(mapHeight),
		_mapWidth(mapWidth), _fullscreen(fullscreen), _numEnemies(numEnemies) {
		_pl.setNumAmmo(numAmmo);
	}

	virtual void PreInit(int& width, int& height, bool& fullscreen)
	{
		width = _winWidth;
		height = _winHeight;
		fullscreen = _fullscreen;
		//to make window centered:
		_winCoordX = (_mapWidth - _winWidth) / 2;
		_winCoordY = (_mapHeight - _winHeight) / 2;
	}

	virtual bool Init() {
		//setting up the player
		if (!_pl.setPlayerSprite(createSprite(R"(data\avatar.jpg)")) ||
			!_pl.setMouseSprite(createSprite(R"(data\circle.tga)"))
			|| !_pl.setBulletSprite(createSprite(R"(data\bullet.png)"))) {
			std::cerr << "error loading sprites\n";
			return false;
		}
		//setting up the enemy's sprite
		enemySpr = createSprite(R"(data\enemy.png)");
		if (!enemySpr) {
			std::cerr << "error loading sprites\n";
			return false;
		}
		//to make the player centered
		_pl.X = (_winWidth - _pl.getPlWidth()) / 2 + _winCoordX;
		_pl.Y = (_winHeight - _pl.getPlHeight()) / 2 + _winCoordY;

		//hide native mouse cursor
		showCursor(false);
		return true;
	}

	virtual void Close() {
		if (enemySpr)
			destroySprite(enemySpr);
	}

	virtual bool Tick() {
		drawTestBackground();
		//drawing player
		drawSprite(_pl.getPlSprite(), _pl.X - _winCoordX, _pl.Y - _winCoordY);
		_movePlayer();
		//creating new enemies
		_handleEnemies();
		/* would return false if collision between enemy and player occurred.
		Restarts the game */
		if (!_moveEnemies()) {
			std::cout << "YOU DIED\n";
			_restart();
		}
		//processes mouse's click
		if (!_isReleased) {
			_pl.shotFired(_pl.mX + _winCoordX, _pl.mY + _winCoordY);
			_isReleased = true;
		}
		//moves all bullets and checks collision between them and enemies
		_moveBullets();
		//mouse's cursor would disappear when shooting otherwise
		drawSprite(_pl.getMsSprite(), _pl.mX, _pl.mY); 
		drawSprite(_pl.getMsSprite(), _pl.mX + _pl.mXX, _pl.mY + _pl.mYY);
		return false;
	}

	virtual void onMouseMove(int x, int y, int xrelative, int yrelative) {
		_pl.mX = x;
		_pl.mY = y;
		_pl.mXX = xrelative;
		_pl.mYY = yrelative;
	}

	virtual void onMouseButtonClick(FRMouseButton button, bool isReleased) {
		if (button == FRMouseButton::LEFT)
			_isReleased = isReleased;
	}

	virtual void onKeyPressed(FRKey k) {
		switch (k) {
		case FRKey::UP:
			_pl.UpPressed = true;
			break;
		case FRKey::DOWN:
			_pl.DownPressed = true;
			break;
		case FRKey::LEFT:
			_pl.LeftPressed = true;
			break;
		case FRKey::RIGHT:
			_pl.RightPressed = true;
			break;
		default:
			break;
		}
	}

	virtual void onKeyReleased(FRKey k) {
		switch (k) {
		case FRKey::UP:
			_pl.UpPressed = false;
			break;
		case FRKey::DOWN:
			_pl.DownPressed = false;
			break;
		case FRKey::LEFT:
			_pl.LeftPressed = false;
			break;
		case FRKey::RIGHT:
			_pl.RightPressed = false;
			break;
		default:
			break;
		}
	}

private:
	Player _pl;
	Sprite * enemySpr = nullptr;
	std::list<std::unique_ptr<MoveableEntity>> _enemies;
	int _winHeight;
	int _winCoordX;
	int _winCoordY;
	int _winWidth;
	int _mapHeight;
	int _mapWidth;
	int _numEnemies;
	bool _fullscreen;
	bool _isReleased = true; //for mouse

#define PL_SPEED 1

	//moves the player accordingly to pressed buttons
	void _movePlayer() {
		if (_pl.UpPressed) {
			if (_winCoordY - PL_SPEED >= 0) {
				_pl.Y -= PL_SPEED;
				_winCoordY -= PL_SPEED;
			}
		}
		if (_pl.DownPressed) {
			if (_winCoordY + _winHeight + PL_SPEED <= _mapHeight) {
				_pl.Y += PL_SPEED;
				_winCoordY += PL_SPEED;
			}
		}
		if (_pl.LeftPressed) {
			if (_winCoordX - PL_SPEED >= 0) {
				_pl.X -= PL_SPEED;
				_winCoordX -= PL_SPEED;
			}
		}
		if (_pl.RightPressed) {
			if (_winCoordX + _winWidth + PL_SPEED <= _mapWidth) {
				_pl.X += PL_SPEED;
				_winCoordX += PL_SPEED;
			}
		}
	}

#define CLOSEST_DIST 100 //no enemy would appear closer
#define DOUBLE_CLOSEST_DIST (CLOSEST_DIST * 2) //used for "throwing enemy further"
#define EN_SPEED 0.090 //basic speed for enemy
#define ADD_EN_SPEED 0.060 //additional maximum speed
#define FREQ_OF_BIRTH 40 //how often new enemies would appear

	//responsible for creating new enemies
	void _handleEnemies() {
		srand(getTickCount());
		if (MoveableEntity::countEnemiesNum <= _numEnemies) {
			if (rand() % 1000 <= FREQ_OF_BIRTH) {
				_enemies.push_back(std::unique_ptr<MoveableEntity>(new MoveableEntity(enemySpr)));
				MoveableEntity::countEnemiesNum++;
				/*std::list<std::unique_ptr<AEnemy>>::iterator*/
				auto en = std::prev(_enemies.end()); //iterator
				(*en)->X = rand() % _mapWidth;
				(*en)->Y = rand() % _mapHeight;
				(*en)->speed = EN_SPEED + std::fmod(rand(), ADD_EN_SPEED); //rand() % ADD_EN_SPEED;
				//makes sure that no enemy would appear too close to the player
				if (std::abs((*en)->X - _pl.X) <= CLOSEST_DIST &&
					std::abs((*en)->Y - _pl.Y) <= CLOSEST_DIST) {
					std::rand() % 100 > 50 ? (*en)->X -= DOUBLE_CLOSEST_DIST :
						(*en)->X += (DOUBLE_CLOSEST_DIST + _pl.getPlWidth());
					std::rand() % 100 > 50 ? (*en)->Y -= DOUBLE_CLOSEST_DIST :
						(*en)->Y += (DOUBLE_CLOSEST_DIST + _pl.getPlHeight());
				}
			}
		}
	}

	//moves enemies towards the player
	bool _moveEnemies() {
		for (auto & i : _enemies) {
			_pl.findVelocity(i->speed, i->X, _pl.X, i->Y, _pl.Y, i->mSpeedX, i->mSpeedY);
			i->X += i->mSpeedX;
			i->Y += i->mSpeedY;
			//draw enemies only if they are inside the window
			if (i->X + i->getWidth() >= _winCoordX &&
				i->X <= _winCoordX + _winWidth &&
				i->Y + i->getHeight() >= _winCoordY &&
				i->Y <= _winCoordY + _winHeight)
				drawSprite(i->getSprite(), (i->X - _winCoordX), (i->Y - _winCoordY));
			if (_checkCollisionPlEn(i))
				return false;
		}
		return true;
	}

	//moves bullets towards their destination. Also it checks collisions.
	void _moveBullets() {
		auto i = _pl.bullets.begin();

		while (i != _pl.bullets.end()) {
			//once the bullet leaves the window it gets destroyed
			if (!((*i)->X + (*i)->getWidth() >= _winCoordX &&
				(*i)->X <= _winCoordX + _winWidth &&
				(*i)->Y + (*i)->getHeight() >= _winCoordY &&
				(*i)->Y <= _winCoordY + _winHeight))
				_pl.bullets.erase(i++);
			//takes care of collisions between the bullet and an enemy
			else if (_checkCollisionBulEn(*i))
				_pl.bullets.erase(i++);
			//moves the bullet
			else {
				(*i)->X += (*i)->mSpeedX;
				(*i)->Y += (*i)->mSpeedY;
				drawSprite((*i)->getSprite(), ((*i)->X - _winCoordX), ((*i)->Y - _winCoordY));
				++i;
			}
		}
	}
	
	//checks collision between bullet and enemy
	bool _checkCollisionBulEn(std::unique_ptr<MoveableEntity> & bul) {
		auto i = _enemies.begin();

		while (i != _enemies.end()) {
			if (bul->X + bul->getWidth() >= (*i)->X &&
				bul->X <= (*i)->X + (*i)->getWidth() &&
				bul->Y + bul->getHeight() >= (*i)->Y &&
				bul->Y <= (*i)->Y + (*i)->getHeight()) {
				_enemies.erase(i++);
				return true;
			}
			else {
				++i;
			}
		}
		return false;
	}

	//checks collision between player and enemy
	bool _checkCollisionPlEn(std::unique_ptr<MoveableEntity> & en) const {
		if (en->X + en->getWidth() >= _pl.X &&
			en->X <= _pl.X + _pl.getPlWidth() &&
			en->Y + en->getHeight() >= _pl.Y &&
			en->Y <= _pl.Y + _pl.getPlHeight())
			return true;
		return false;
	}

	//restarts the game
	void _restart() {
		_winCoordX = (_mapWidth - _winWidth) / 2;
		_winCoordY = (_mapHeight - _winHeight) / 2;
		_pl.X = (_winWidth - _pl.getPlWidth()) / 2 + _winCoordX;
		_pl.Y = (_winHeight - _pl.getPlHeight()) / 2 + _winCoordY;
		MoveableEntity::countEnemiesNum = 0; //keeps track of how many enemies are actually in the game
		MoveableEntity::countBulletsNum = 0; //keeps track of how many shots were made
		_enemies.clear();
		_pl.bullets.clear();
	}
};

bool checkParameters(ParseCommandArgs & parser) {
	if (parser.getMapHeight() <= parser.getWindowHeight() || parser.getMapWidth() <=
		parser.getWindowWidth()) {
		std::cerr << "map can't be smaller than window\n";
		return false;
	}
	return true;
}

int main(int argc, char *argv[])
{
	//stoi might throw
	try {
		ParseCommandArgs parser(argc, argv);
		if (!checkParameters(parser))
			exit(1);

		auto fr = std::unique_ptr<Framework>(new MyFramework(parser.getWindowHeight(),
			parser.getWindowWidth(), parser.getMapHeight(),
			parser.getMapWidth(), parser.getFullscreen(),
			parser.getNumEnemies(), parser.getNumAmmo()));

		return run(fr.get());
	}
	catch (std::exception & e) {
		std::cerr << "Exception occurred: " << e.what() << '\n';
	}
}
